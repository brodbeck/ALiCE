﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class EmergencyCallSound : MonoBehaviour {

	public List<AudioClip> clips;
	public AudioSource	soundSource = null;
	public Flowchart flow = null;
	private int _isPlaying = -1;


	// Use this for initialization
	void Start () {	
	}
	
	// Update is called once per frame
	void Update () {
		if(_isPlaying != -1) {
			if(soundSource.isPlaying == false) {
				Debug.Log("Alice Emergency Call fertig: " + _isPlaying);
				flow.SetIntegerVariable("PassedDialog", _isPlaying+1);
				_isPlaying = -1;
			}
		}
	}

	void Play1() 
	{
		soundSource.clip = clips[0];
		soundSource.Play();
		_isPlaying = 1;
	}
	void Play2() 
	{
		soundSource.clip = clips[1];
		soundSource.Play();
		_isPlaying = 2;
	}
	void Play3() 
	{
		soundSource.clip = clips[2];
		soundSource.Play();
		_isPlaying = 3;
	}
	void Play4() 
	{
		soundSource.clip = clips[3];
		soundSource.Play();
		_isPlaying = 4;
	}
	void Play5() 
	{
		soundSource.clip = clips[4];
		soundSource.Play();
		_isPlaying = 5;
	}
	void Play6() 
	{
		soundSource.clip = clips[5];
		soundSource.Play();
		_isPlaying = 6;
	}
	void Play7() 
	{
		soundSource.clip = clips[6];
		soundSource.Play();
		_isPlaying = 7;
	}
	void Play8() 
	{
		soundSource.clip = clips[7];
		soundSource.Play();
		_isPlaying = 8;
	}
	void Play9() 
	{
		soundSource.clip = clips[8];
		soundSource.Play();
		_isPlaying = 9;
	}

	
}
