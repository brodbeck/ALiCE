﻿using UnityEngine;
using System.Collections;

public class ShowHandy : MonoBehaviour
{
    [SerializeField]
    public GameObject handy;
   
    public bool showhandy;
   
    void Start()
    {
        showhandy = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (showhandy == false)
        {
            handy.SetActive(false);
        }
        if (showhandy == true)
        {
            handy.SetActive(true);
        }
      
    }
    void ShowSmartphone()
    {
        showhandy = true;
    }

    void HideSmartphone()
    {
        showhandy = false;
    }
}