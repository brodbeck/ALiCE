﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class FellDowns : MonoBehaviour {

    public Flowchart flow;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void AnimationEnded()
    {
        anim.SetBool("FellDown", true);
        flow.SetBooleanVariable("FellDown", true);
    }
}
