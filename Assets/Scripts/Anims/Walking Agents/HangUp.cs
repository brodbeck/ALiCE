﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
public class HangUp : MonoBehaviour
{


    public Flowchart flow;
    Animator anim;

    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (flow.GetBooleanVariable("AliceHangUp"))
        {
           HangUps(currentAnimationName(), 37.0f);
            flow.SetBooleanVariable("AliceHangUp", false);
        }

    }

    void HangUps(string name, float nTime)
    {
        anim.Play(name, 0, nTime);
    }

    string currentAnimationName()
    {
        var currAnimName = "";
        foreach (AnimationClip clip in anim.runtimeAnimatorController.animationClips)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName(clip.name))
            {
                currAnimName = clip.name.ToString();
            }
        }

        return currAnimName;

    }
}
