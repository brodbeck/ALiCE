﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
public class GotUps : MonoBehaviour {


    public Flowchart flow;
    Animator anim;

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void AnimationEnded()
    {
        if(anim.name == "OlderWoman01")
        {
            anim.SetBool("GotUp", true);
            flow.SetBooleanVariable("LadyGotUp", true);
        }

        if (anim.name == "YoungWoman01")
        {
            anim.SetBool("GotUp", true);
            flow.SetBooleanVariable("MuttiGotUp", true);
        }

        if (anim.name == "Alice01")
        {
            anim.SetBool("GotUp", true);
            flow.SetBooleanVariable("AliceGotUp", true);
        }

        if (anim.name == "OlderMan02")
        {
            anim.SetBool("GotUp", true);
            flow.SetBooleanVariable("GuyGotUp", true);
        }

        // anim.SetBool("GotUp", true);
        //flow.SetBooleanVariable("GotUp", true);
    }
}
