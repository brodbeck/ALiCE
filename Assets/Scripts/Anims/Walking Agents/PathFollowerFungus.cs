﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
public class PathFollowerFungus : MonoBehaviour
{
    RaycastHit hitInfo = new RaycastHit();
    UnityEngine.AI.NavMeshAgent agent;

    public Transform[] path;
    public float reachDist = 0.1f;
    public int currentPoint = 0;

    Animator anim;


    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

    }

    // Update is called once per frame
    void Update()
    {
        if (anim.GetBool("pos" + currentPoint))
        {
            Ray ray = Camera.main.ScreenPointToRay(path[currentPoint].position);
            if (Physics.Raycast(ray.origin, ray.direction, out hitInfo))
                agent.destination = path[currentPoint].position;

        }
        float dist = Vector3.Distance(path[currentPoint].position, transform.position);



        //if you're close enough to destination, choose next destination, plz
        if (dist <= reachDist)
        {
            anim.SetBool("pos" + currentPoint, false);
            currentPoint++;
        }

        //let's not get out of bounds, puhleaze!
        if (currentPoint >= path.Length)
        {
            currentPoint = 0;
        }

        
    }

    private void OnDrawGizmos()
    {
        if (path.Length > 0)
        {
            for (int i = 0; i < path.Length; i++)
            {
                if (path[i] != null)
                {
                    Gizmos.DrawSphere(path[i].position, 0.2f);
                }
            }
        }
    }
}
