﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundObject : MonoBehaviour {
	public AudioSource sound = null;

	public void Start() {
		sound = GetComponent<AudioSource>();
	}

	public void Play() {
		if (sound != null) {
			sound.Play();
		}
	}

	public void Stop() {
		if(sound != null) {
			sound.Stop();
		}
	}
}
