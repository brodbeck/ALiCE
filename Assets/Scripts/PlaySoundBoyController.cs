﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundBoyController : MonoBehaviour {

	private PlaySoundBoy sound = null;
	void Start () {
		sound = GetComponent<PlaySoundBoy>();
	}
	
	void PlayCoughing() {
		sound.PlayCoughing();
	}

	void PlayWheezing()  {
		sound.PlayWheezing();
	}
	
}
