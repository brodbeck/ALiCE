﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundBoy: MonoBehaviour {

	public AudioSource coughing;
	public AudioSource wheezing;
	public AudioSource falling;
	public void PlayCoughing() {
		if(coughing != null) {
			coughing.Play();
		}
	}

	public void PlayWheezing() {
		if(wheezing != null) {
			Debug.Log("Wheezing Play");
			wheezing.Play();
		}
	}

	public void PlayFalling()
	{
		if(falling != null) {
			falling.Play();
		}
	}

	public void StopCoughing() {
		if(coughing != null) {
			coughing.Stop();
		}
	}

	public void StopWheezing() {
		if(coughing != null) {
			wheezing.Stop();
			wheezing.Stop();
		}
	}

	public void StopFalling()
	{
		if(falling != null) {
			falling.Stop();
		}
	}



}
