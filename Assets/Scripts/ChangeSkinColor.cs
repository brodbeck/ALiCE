﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSkinColor : MonoBehaviour {

	public List<Material> materials = new List<Material>();
	public SkinnedMeshRenderer characterMesh;
	public double delayNextInSec = 1.0;
	private int _current = 0;
	private int _max;

	private int _repeatNext = 0;
	private double _timer = 0;


	// Use this for initialization
	void Start () {
		_max = materials.Count-1;
	}
	
	// Update is called once per frame
	void Update () {
		if(_repeatNext > 0) {
			if(_timer > 0) {
				_timer -= Time.deltaTime;
			} else {
				_timer = delayNextInSec;
				_repeatNext--;
				next();
			}
		}
		if(Input.GetKeyDown("n")) {
			next();
		}
		if(Input.GetKeyDown("m")) {
			pre();
		}
	}

	void next() {
		_current++;
		if(_current > _max) {
			_current = _max;
		}
		characterMesh.material = materials[_current];
	}

	void timedNext(int repeatNext) {
		_repeatNext += repeatNext;		
	}

	void nextPhaseOne() {
		timedNext(10);
	}

	void nextPhaseTwo()
	{
		timedNext(6);
	}

	void nextPhaseThree()
	{
		timedNext(3);
	}

	void pre() {
		_current--;
		if(_current < 0) {
			_current = 0;
		}
		characterMesh.material = materials[_current];
	}
}
