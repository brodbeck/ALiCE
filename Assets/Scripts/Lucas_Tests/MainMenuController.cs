﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour {
    
    public void LoadAllergy()
    {
        SceneManager.LoadScene("Simulation014", LoadSceneMode.Single);
    }

    public void LoadDiabetes()
    {
        SceneManager.LoadScene("Simulation014", LoadSceneMode.Single);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
	
}
