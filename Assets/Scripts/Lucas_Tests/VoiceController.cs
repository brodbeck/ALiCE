﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Windows.Speech;

//from https://www.youtube.com/watch?v=XttwozSJGMQ
//and Unity https://docs.unity3d.com/ScriptReference/Windows.Speech.PhraseRecognitionSystem.html
//and Microsoft https://developer.microsoft.com/en-us/windows/mixed-reality/voice_input_in_unity
public class VoiceController : MonoBehaviour
{
	//hh
    [SerializeField]
    private string[] m_Keywords;

    private KeywordRecognizer m_Recognizer;
    public GameObject Cube;
    public GameObject Sphere;

    void Start()
    {
        m_Keywords = new string[2];
        m_Keywords[0] = "Ja";
        m_Keywords[1] = "Nein";
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;
        m_Recognizer.Start();
    }

    private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
        if (args.text == m_Keywords[0]) 
        {
            Instantiate(Cube, new Vector3(-3, 4, 1), Quaternion.identity);
        }

        if(args.text == m_Keywords[1])
        {
            Instantiate(Sphere, new Vector3(5, 4, 3), Quaternion.identity);
        }
    }
}
