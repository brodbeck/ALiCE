﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Windows.Speech;
using Fungus;

//from https://www.youtube.com/watch?v=XttwozSJGMQ
public class VoiceControllerB : MonoBehaviour
{

    [SerializeField]
    private string[] m_Keywords;
	private bool wantsToOrder;
    private KeywordRecognizer m_Recognizer;
	private bool _validOrderInput = false;
	private bool _doesNotWantToOrder = false;

	private bool saidYes = false;

    public Flowchart flow;
    static Animator anim;

    void Start()
    {
		//get Francesco on the line
        anim = GetComponent<Animator>();

		//initialize the array with 8 different options for the voice commands
        m_Keywords = new string[11];

		//initialize the explicit voice commands 
        m_Keywords[0] = "No";
        m_Keywords[1] = "Nein";
		m_Keywords[8] = "Nicht";
		m_Keywords[9] = "Moment";
		m_Keywords[10] = "Time";
        m_Keywords[2] = "Wasser";
		m_Keywords [3] = "Tee";
		m_Keywords [4] = "Kaffee";
		m_Keywords [5] = "Kakao";
		m_Keywords [6] = "Fanta";
		m_Keywords [7] = "Cola";

		//initialize the recognizer with the array, method and start it
        m_Recognizer = new KeywordRecognizer(m_Keywords);
        m_Recognizer.OnPhraseRecognized += OnPhraseRecognized;
        
    }

	public void StartRecognition() {
		m_Recognizer.Start();
	}

	public void StopRecognition() {
		m_Recognizer.Stop();
	}

    private void OnPhraseRecognized(PhraseRecognizedEventArgs args)
    {
		Debug.Log(args.text);

		if(args.text == m_Keywords[1] || args.text == m_Keywords[8] || args.text == m_Keywords[9]
			|| args.text == m_Keywords[10]) {
			_doesNotWantToOrder = true;
			wantsToOrder = false;
			_validOrderInput = true;
			Debug.Log("Möchte nicht bestellen");
		}

		if(args.text == m_Keywords[2] || args.text == m_Keywords[3] || args.text == m_Keywords[4] 
			|| args.text == m_Keywords[5] || args.text == m_Keywords[6] || args.text == m_Keywords[7])
			{
				_doesNotWantToOrder = false;
				_validOrderInput = true;
				Debug.Log("Hat bestellt");
			}
		

		if(_validOrderInput) {
			setBooleanInFlow("ValidVoiceInput");
			Debug.Log("Valid Voice");
			if(_doesNotWantToOrder) {
				setBooleanInFlow("OrderNeedMoreTime");
			}
		}
    }

	private void setBooleanInFlow(String item){
		flow.SetBooleanVariable(item, true);
	}
}
