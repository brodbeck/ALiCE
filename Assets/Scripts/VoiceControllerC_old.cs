﻿	using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using UnityEngine;
using UnityEngine.Windows.Speech;
using Fungus;

//from https://www.youtube.com/watch?v=XttwozSJGMQ
public class VoiceControllerC_old : MonoBehaviour
{

    [SerializeField]
    private string[] _keywords;
    private DictationRecognizer m_Recognizer;
	private bool _sayActive = true;
	private bool _doesNotWantToOrder = false;
	private bool _orderedFood = false;
	private bool _orderedDrink = false;
	private bool _orderFinished = false;
	private bool _saidYes = false;
	private bool _menuInfoSaid = false;
	public List<AudioClip> _clips;
	public SayDialog dialog = null;
    public Flowchart flow;

    void Start()
    {
		// Kakao gestrichen und Tee (ToDo!) Karte auch nen ToDo!

        _keywords = new string[21];
		//initialize the explicit voice commands 
        _keywords[0] = "ja";
        _keywords[1] = "nein";
		_keywords[8] = "nicht";
		_keywords[9] = "moment";
		_keywords[10] = "zeit";
        _keywords[2] = "wasser";
		_keywords [4] = "kaffee";
		_keywords [5] = "kakao";
		_keywords [6] = "fanta";
		_keywords[20] = "salat";
		_keywords[12] = "pasta";
		_keywords[13] = "pizza";
		_keywords [7] = "cola";
		_keywords[11] = "joa";
		_keywords[14] = "haben";
		_keywords[15] = "karte";
		_keywords[16] = "gibt";
		_keywords[17] = "ham";
		_keywords[18] = "jo";
		_keywords[19] = "jow";


		//initialize the recognizer with the array, method and start it
        m_Recognizer = new DictationRecognizer();
        m_Recognizer.DictationResult += (text, confidence) =>
            {
                Recognize(text);
            };
    }

	public void Update() {
		if(Input.GetKeyUp("u")) {
			_sayActive = !_sayActive;
		}

		if(Input.GetKeyUp("v")) {
			_doesNotWantToOrder = true;
			setBooleanInFlow("ValidVoiceInput");
			Debug.Log("Bestellprozess übersprungen");
		}
	}

	public void StartRecognition() {
		
		Debug.Log("Recognition started");
		m_Recognizer.Start();
	}

	public void StopRecognition() {
		Debug.Log("Recognition stopped");
		m_Recognizer.Stop();
	}

	private void Recognize(string recognized) {
		string text = recognized.ToLower();
		Debug.LogFormat("Dictation result: {0}", text);

		if(!_menuInfoSaid && (text.Contains(_keywords[14]) || text.Contains(_keywords[17]) || text.Contains(_keywords[15]) || text.Contains(_keywords[16]))) {
			_menuInfoSaid = true;
			Debug.Log("Info-Pfad");
			Say("Zurzeit gilt nur die Mittagskarte, also Pizza oder Pasta." + 
			" Getränke natürlich die gesamte Karte. Cola, Fanta, Wasser, Kakao oder auch Kaffee.", 0);
		}

		// Wenn Person nein, zeit, moment oder ähnliches sagt
		if(text.Contains(_keywords[1]) || text.Contains(_keywords[8]) || text.Contains(_keywords[9])
			|| text.Contains(_keywords[10])) {
			if(!_orderedFood && !_orderedDrink)
			{	
				_doesNotWantToOrder = true;
			
			} else {
				_orderFinished = true;
			}
			Debug.Log("Nein-Pfad");
		}

		// wenn person etwas wie cola oder ähnliches zeit
		if(!_orderedDrink && (text.Contains(_keywords[2]) || text.Contains(_keywords[4]) 
			|| text.Contains(_keywords[5]) || text.Contains(_keywords[6]) || text.Contains(_keywords[7])))
			{	
				_orderedDrink = true;
				if(!_orderedFood) {
					Say("Salat, Pasta oder Pizza dazu?", 1);
				}
				if(_orderedFood && _orderedDrink) {
					_orderFinished = true;
				}
				Debug.Log("Trinken bestellung");
			}

		// Person  sagt nur joa oder ja!
		if(!_saidYes && (text.Contains(_keywords[0]) || text.Contains(_keywords[11]) || text.Contains(_keywords[18]) || text.Contains(_keywords[19]))) {
			Debug.Log("Ja-Pfad");
			Say("Was möchten Sie denn haben?", 14);
			_saidYes = true;
		}

		if(!_orderedFood && (text.Contains(_keywords[12]) || text.Contains(_keywords[13]) || text.Contains(_keywords[20]))) {
			Debug.Log("Essen bestellt Pfad");
			_orderedFood = true;
			if(_orderedFood && _orderedDrink) {
				_orderFinished = true;
			} else {
				Say("Noch etwas zu trinken dazu?", 5);
			}
		}

		if(_orderFinished || _doesNotWantToOrder) {
			Debug.Log("Finish-Pfad");
			setBooleanInFlow("ValidVoiceInput");
			if(_orderedDrink || _orderedFood) {
				setBooleanInFlow("OrderedSth");
			}
			if(_doesNotWantToOrder) {
				setBooleanInFlow("OrderNeedMoreTime");
			}	
		}
	}

	private void setBooleanInFlow(String item){
		flow.SetBooleanVariable(item, true);
	}

	private void Say(string text, int VONumber) {
		if(dialog != null) {
			if(!_sayActive) {
				text = "";
			}
			dialog.gameObject.SetActive(true);
			dialog.Say(text, true, false, true, false, true, _clips[VONumber], null);
		}
	}
}
