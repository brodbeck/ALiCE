﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Interactions : MonoBehaviour {

    public void Getraenkekarte_OnClick(string produkt)
    {
        switch(produkt) {
            case "Wasser":
                Debug.Log("Du hast Wasser bestellt");
                break;
            case "Cola":
                Debug.Log("Du hast Cola bestellt");
                break;
            case "Tee":
                Debug.Log("Du hast Tee bestellt");
                break;
            case "Kaffee":
                Debug.Log("Du hast Kaffee bestellt");
                break;
            case "Kakao":
                Debug.Log("Du hast Kakao bestellt");
                break;
            case "Fanta":
                Debug.Log("Du hast Fanta bestellt");
                break;
            default:
                Debug.Log("Produkt nicht bekannt");
                break;
        }
        
    }
}
