﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class EpipenUse : MonoBehaviour {

	public Collider collideWith;
	public Flowchart flowchartWithVar;

	void OnTriggerEnter(Collider other) {
		if (other == collideWith) {
			Use();
		}
    }

	private void Use() {
		flowchartWithVar.SetBooleanVariable("EpipenGegeben", true);
        this.gameObject.SetActive(false);
	}

	void Update() {
		if(Input.GetKeyDown("e")) {
			Use();
		}
	}
}
