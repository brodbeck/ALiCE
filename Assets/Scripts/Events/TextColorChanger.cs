﻿using UnityEngine;
 using System.Collections;
 using UnityEngine.EventSystems;
 using UnityEngine.UI;
 
 [RequireComponent (typeof(Button))]
 public class TextColorChanger : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
 {
     // János: Hatte eig was eigenes, aber das ist ausführlicher und automatisch. Hätte man selbst schreiben können, bin ich aber nun paar Tage später drauf gestoßen.
     // geiles Skript. Quelle: https://answers.unity.com/questions/940456/how-to-change-text-color-on-hover-in-new-gui.html
     // Müssen wir eventuell noch für die Farbgenerierung anpassen wenn es uns farblich nicht passt
     
     Text txt;
     Color baseColor;
     Button btn;
     bool interactableDelay;
 
     void Start ()
     {
         txt = GetComponentInChildren<Text>();
         baseColor = txt.color;
         btn = gameObject.GetComponent<Button> ();
         interactableDelay = btn.interactable;
     }
 
     void Update ()
     {
         if (btn.interactable != interactableDelay) {
             if (btn.interactable) {
                 txt.color = baseColor * btn.colors.normalColor * btn.colors.colorMultiplier;
             } else {
                 txt.color = baseColor * btn.colors.disabledColor * btn.colors.colorMultiplier;
             }
         }
         interactableDelay = btn.interactable;
     }
 
     public void OnPointerEnter (PointerEventData eventData)
     {
         if (btn.interactable) {
             txt.color = baseColor * btn.colors.highlightedColor * btn.colors.colorMultiplier;
         } else {
             txt.color = baseColor * btn.colors.disabledColor * btn.colors.colorMultiplier;
         }
     }
 
     public void OnPointerDown (PointerEventData eventData)
     {
         if (btn.interactable) {
             txt.color = baseColor * btn.colors.pressedColor * btn.colors.colorMultiplier;
         } else {
             txt.color = baseColor * btn.colors.disabledColor * btn.colors.colorMultiplier;
         }
     }
 
     public void OnPointerUp (PointerEventData eventData)
     {
         if (btn.interactable) {
             txt.color = baseColor * btn.colors.highlightedColor * btn.colors.colorMultiplier;
         } else {
             txt.color = baseColor * btn.colors.disabledColor * btn.colors.colorMultiplier;
         }
     }
 
     public void OnPointerExit (PointerEventData eventData)
     {
         if (btn.interactable) {
             txt.color = baseColor * btn.colors.normalColor * btn.colors.colorMultiplier;
         } else {
             txt.color = baseColor * btn.colors.disabledColor * btn.colors.colorMultiplier;
         }
     }
 
 }