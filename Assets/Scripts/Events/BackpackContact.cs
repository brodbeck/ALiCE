﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class BackpackContact : MonoBehaviour {

	public Flowchart flowchartWithVar= null;
	public GameObject epipen;
	public AudioSource soundOpen;

	public void Activate() {
		if(flowchartWithVar != null && epipen != null) {
			if(flowchartWithVar.GetBooleanVariable("PlayerBeiKind") && flowchartWithVar.GetBooleanVariable("Hustet") &&
				!flowchartWithVar.GetBooleanVariable("RucksackGeoffnet")) {
				flowchartWithVar.SetBooleanVariable("RucksackGeoeffnet", true);
				soundOpen.Play();
				SpawnEpipen();
			}
		}
	}

	private void SpawnEpipen() {
		epipen.SetActive(true);
	}

	void Update() {
		if(Input.GetKeyDown("b")) {
			Activate();
		}
	}
}
