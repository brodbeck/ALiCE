﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class CollisionTrigger : MonoBehaviour {

    public Flowchart flow;
    public Collider player;
   // static Animator anim;
    // Use this for initialization
    void Start()
    {
       // anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {   
        if(player == null || other == player) {
            //anim.SetTrigger("sitzt");
            flow.SetBooleanVariable("SitzErreicht", true);
        }
    }
}
