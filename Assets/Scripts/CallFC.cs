﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class CallFC : MonoBehaviour {

	private bool _activate = false;
	private int _zahl = 0;
	public Flowchart flow = null;
	// Use this for initialization
	void Start () {
	}

	public void Do() {
		_activate = true;
	}


	
	// Update is called once per frame
	void Update () {
		if(_activate) {
			if(flow != null) {
				Debug.Log("Flowchart aufgerufen");
				flow.ExecuteBlock("New Block");
			}
		}
	}
}
