﻿namespace VRTK
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using Fungus;

    [AddComponentMenu("VRTK/Scripts/Presence/VRTK_HeadsetCollisionFade")]
    public class FadePlease : MonoBehaviour
    {

        public Flowchart flow;

        [Tooltip("The fade blink speed on collision.")]
        public float blinkTransitionSpeed = 0.1f;
        [Tooltip("The colour to fade the headset to on collision.")]
        public Color fadeColor = Color.black;

        public VRTK_HeadsetFade headsetFade;

        // Use this for initialization
        void Start()
        {
            
        }

        // Update is called once per frame
        void Update()
        {
        }

        void FadeNowPlease()
        {
            headsetFade.Fade(fadeColor, blinkTransitionSpeed);
            Debug.Log("We're fading");
        }
    }
}
