﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOnStart : MonoBehaviour {

	public List<GameObject> ToEnable = new List<GameObject>();

	void Start () {
		foreach(GameObject gObj in ToEnable) {
			gObj.SetActive(true);
		}
	}
}
