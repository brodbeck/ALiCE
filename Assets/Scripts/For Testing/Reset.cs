﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reset : MonoBehaviour {
	// Setzt ein beliebiges Objekt mit 'R' zurück an eine festgelegte Position
	public float x,y,z;
	public Transform ToReset;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("r")) {
			ToReset.position = new Vector3(x,y,z);
		}
	}
}
