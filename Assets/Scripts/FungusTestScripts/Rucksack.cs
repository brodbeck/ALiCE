﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
public class Rucksack : MonoBehaviour {

    private bool _activate = false;
    public Flowchart ursprung;
    public Flowchart ziel;

    // Use this for initialization
    void Start () {
		
	}

    public void ActivateRucksack()
    {
        _activate = true;
        Debug.Log("DO!");
    }
    // Update is called once per frame
    void Update () {
        if (_activate)
        {
            if (ursprung.GetBooleanVariable("PlayerBeiKind"))
            {
                if (ziel != null)
                {
                   // Debug.Log("Flowchart Ziel aufgerufen");
                    ziel.ExecuteBlock("New Block");
                }
            }
        }
    }
}
