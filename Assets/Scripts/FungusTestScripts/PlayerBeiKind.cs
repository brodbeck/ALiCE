﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class PlayerBeiKind : MonoBehaviour {

    public Flowchart flow;
	public Flowchart flowRucksack;
    public Transform player;
    private bool _active = false;

    // Use this for initialization
    void Start()
    {
    }

    public void ActivateKindLiegt()
    {
        _active = true;
        Debug.Log("DO!");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (_active)
        {
            flow.SetBooleanVariable("PlayerBeiKind", true);
        }
    }
}
