﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class EpiTrue : MonoBehaviour {
    public Flowchart flow;
    public Flowchart rucksack;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("space"))
        {
            if (flow.GetBooleanVariable("Hustet"))
            {
                if (rucksack.GetBooleanVariable("Rucksack"))
                {
                    flow.SetBooleanVariable("EpipenGegeben", true);
                }
            }
        }
    }
}
