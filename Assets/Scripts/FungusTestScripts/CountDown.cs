﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;
public class CountDown : MonoBehaviour {
    public float timeLeft_hustet;
    public float timeLeft_Atemnot;
    public float timeLeft_trueb;
    public float timeLeft_mutti;
    
    public PlaySoundBoy playSoundBoy = null;
    public Flowchart flow;
    public Flowchart flow_mutti;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (flow.GetBooleanVariable("EpipenGegeben"))
        {
                flow.ExecuteBlock("Besser");
        }

        //Countdown bis husten
        if (flow.GetBooleanVariable("Los") && !(flow.GetBooleanVariable("Hustet")))
        {
            timeLeft_hustet -= Time.deltaTime;
            //Debug.Log("Countdown bis Hustet: " + Mathf.Round(timeLeft_hustet));
            if (timeLeft_hustet < 0)
            {
                flow.ExecuteBlock("Hustet");
            }
        }

        //countdown bis atmenot
        else if (flow.GetBooleanVariable("Hustet") && !(flow.GetBooleanVariable("Atemnot")))
        {
            timeLeft_Atemnot -= Time.deltaTime;
           // Debug.Log("Countdown bis Atemnot: " + Mathf.Round(timeLeft_Atemnot));
            if (timeLeft_Atemnot < 0)
            {
                flow.ExecuteBlock("Atemnot");
                flow.ExecuteBlock("Alice Timer");
                flow.ExecuteBlock("Alice Gets Up");
                flow.ExecuteBlock("Lady and Guy Get Up");
                flow.ExecuteBlock("Kellner kommt zum Kind");
            }
        }


        //countdown bis trüb
        else if (flow.GetBooleanVariable("Atemnot") && !(flow.GetBooleanVariable("Trueb")))
        {
            timeLeft_trueb -= Time.deltaTime;
          //  Debug.Log("Countdown bis Trueb: " + Mathf.Round(timeLeft_trueb));
            if (timeLeft_trueb < 0)
            {
                playSoundBoy.PlayWheezing();
                flow.ExecuteBlock("Trueb");
               
            }
        }


        //countdown bis besser
        else if (flow.GetBooleanVariable("Trueb") && !(flow.GetBooleanVariable("Besser")))
        {
            timeLeft_mutti -= Time.deltaTime;
            //Debug.Log("Countdown bis Mutti kommt: " + Mathf.Round(timeLeft_mutti));
            if (timeLeft_mutti < 0)
            {
                flow_mutti.ExecuteBlock("Mutti kommt");
            }
        }

    }
   
    
}
