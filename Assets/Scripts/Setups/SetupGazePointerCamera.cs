﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SetupGazePointerCamera : MonoBehaviour {

	public List<GameObject> EyeCameras;
	private VRTK_TransformFollow _follower;
	void Start () {
		_follower = (VRTK_TransformFollow) this.GetComponent("VRTK_TransformFollow");
		if(_follower == null) {
			this.enabled = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (EyeCameras.Count > 0) {
			foreach(GameObject obj in EyeCameras) {
				if (obj != null && obj.activeSelf == true && obj.activeInHierarchy == true) {
					_follower.gameObjectToFollow = obj;
					this.enabled = false;
					break;
				}
			}
		} else 
		{
			this.enabled = false;
		}
	}
}
