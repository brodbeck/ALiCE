﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class SpeisekarteController : MonoBehaviour {
	public Flowchart flow;
	static Animator anim;

	//copy paste from VoiceControllerB.cs and adapted with meals
	private string[] m_Keywords = new string[14];
	private bool gaveItsPreferences;
	//can only order one meal
	private bool orderedAMeal = false;
    private bool orderedDrinkOnMenu = false;

	void Start(){
		//initialize the explicit options, the first two entries stay for no reason
		m_Keywords = new string[14];
		m_Keywords[0] = "Ja";
		m_Keywords[1] = "Nein";
		m_Keywords[2] = "Wasser";
		m_Keywords [3] = "Tee";
		m_Keywords [4] = "Kaffee";
		m_Keywords [5] = "Kakao";
		m_Keywords [6] = "Fanta";
		m_Keywords [7] = "Cola";
		//meals
		m_Keywords [8] = "Pizza";
		m_Keywords [9] = "Pasta";
		m_Keywords [10] = "Fisch";
		m_Keywords [11] = "Fleisch";
		m_Keywords [12] = "Lamm";
		m_Keywords [13] = "Vegan";

		//if participant already gave its preferences using voice command only meal menu available
		gaveItsPreferences = flow.GetBooleanVariable ("VoiceCommand");


	}


	public void OrderWater(){
		if (!gaveItsPreferences) {
			flow.SetBooleanVariable (m_Keywords[2], true);
			flow.SetBooleanVariable("WantsToDrink", true);
			gaveItsPreferences = !gaveItsPreferences;
			print ("Menu " + m_Keywords [2]);
		}
	}

	public void OrderTea(){
		if (!gaveItsPreferences) {
			flow.SetBooleanVariable (m_Keywords[3], true);
			flow.SetBooleanVariable("WantsToDrink", true);
			gaveItsPreferences = !gaveItsPreferences;
			print ("Menu " + m_Keywords [3]);
		}
	}

	public void OrderCoffee(){
		if (!gaveItsPreferences) {
			flow.SetBooleanVariable (m_Keywords[4], true);
			flow.SetBooleanVariable("WantsToDrink", true);
			gaveItsPreferences = !gaveItsPreferences;
			print ("Menu " + m_Keywords [4]);
		}
	}

	public void OrderCacao(){
		if (!gaveItsPreferences) {
			flow.SetBooleanVariable (m_Keywords[5], true);
			flow.SetBooleanVariable("WantsToDrink", true);
			gaveItsPreferences = !gaveItsPreferences;
			print ("Menu " + m_Keywords [5]);
		}
	}

	public void OrderFanta(){
		if (!gaveItsPreferences) {
			flow.SetBooleanVariable (m_Keywords[6], true);
			flow.SetBooleanVariable("WantsToDrink", true);
			gaveItsPreferences = !gaveItsPreferences;
			print ("Menu " + m_Keywords [6]);
		}
	}

	public void OrderCoke(){
		if (!gaveItsPreferences) {
			flow.SetBooleanVariable (m_Keywords[7], true);
			flow.SetBooleanVariable("WantsToDrink", true);
			gaveItsPreferences = !gaveItsPreferences;
			print ("Menu " + m_Keywords [7]);
		}
	}

	public void OrderPizza(){
		if (!orderedAMeal) {
			flow.SetBooleanVariable (m_Keywords[8], true);
			flow.SetBooleanVariable("WantsToEat", true);
			orderedAMeal = !orderedAMeal;
			print ("Menu " + m_Keywords [8]);
		}
	}

	public void OrderPasta(){
		if (!orderedAMeal) {
			flow.SetBooleanVariable (m_Keywords[9], true);
			flow.SetBooleanVariable("WantsToEat", true);
			orderedAMeal = !orderedAMeal;
			print ("Menu " + m_Keywords [9]);
		}
	}

	public void OrderFish(){
		if (!orderedAMeal) {
			flow.SetBooleanVariable (m_Keywords[10], true);
			flow.SetBooleanVariable("WantsToEat", true);
			orderedAMeal = !orderedAMeal;
			print ("Menu " + m_Keywords [10]);
		}
	}

	public void OrderMeat(){
		if (!orderedAMeal) {
			flow.SetBooleanVariable (m_Keywords[11], true);
			flow.SetBooleanVariable("WantsToEat", true);
			orderedAMeal = !orderedAMeal;
			print ("Menu " + m_Keywords [11]);
		}
	}

	public void OrderLamb(){
		if (!orderedAMeal) {
			flow.SetBooleanVariable (m_Keywords[12], true);
			flow.SetBooleanVariable("WantsToEat", true);
			orderedAMeal = !orderedAMeal;
			print ("Menu " + m_Keywords [12]);
		}
	}

	public void OrderVegan(){
		if (!orderedAMeal) {
			flow.SetBooleanVariable (m_Keywords[13], true);
			flow.SetBooleanVariable("WantsToEat", true);
			orderedAMeal = !orderedAMeal;
			print ("Menu " + m_Keywords [13]);
		}
	}


}
