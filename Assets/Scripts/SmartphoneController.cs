﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class SmartphoneController : MonoBehaviour {

    //for testing in simulator only, will be removed in production mode
    private bool doOnce = true;

	public Flowchart flow;
	public Animator anim;
    public GameObject mobileOnTable;

    public void CallEmergency()
    {
        if (doOnce && flow.GetBooleanVariable("AliceGotUp"))
        {
			flow.SetBooleanVariable ("AliceCallsTheEmergency", true);
			print ("Alice takes over to call the emergency");
            doOnce = false;

            //mobile phone placed next to Alice's head, more or less
            //mobileOnTable.transform.rotation = Quaternion.Euler(90, 0, 220);
            //unable to perform correct rotation
            //mobileOnTable.transform.Translate(new Vector3(2.534f, 1.4216f, 1.558f));
            

        }

        //todo play sound and action


        
    }
}
